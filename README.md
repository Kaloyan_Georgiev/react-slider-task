# React Slider Task:
Create a slider by the given design of Figma:
- [ ] [SLIDER DESIGN](https://www.figma.com/file/idLkwWaNJhL1K5cYzJTvb8/AS-Homepage?node-id=0%3A1)

## Task explanation:
You are free to use any library to create the slider but functionality and styling must be exact as on the design. We are using (CSS) for styling manually.
Each slide consists of a title, description, button, and photo. Buttons on all sliders must redirect to "/". The buttons at the bottom should correspond to each slide. 

### Example: 
When you press the first button, it becomes active and changes its color (as in the design), all sliders rotate and the middle slide corresponds to the pressed button as content. Use the same logic for the other four buttons.

### Push code:
When you are ready push the code to GitLab and as a name please use the same name you used to correspond with us. 

## HAPPY CODING!